//
// Created by Anatoly Myaskov on 26/01/2020.
//

#include "can.h"
#include "stm32f1xx_hal_can.h"

typedef struct {
    uint32_t std_id;
    uint32_t ext_id;
    uint32_t ide;
    uint32_t rtr;
    uint32_t dlc;
    unsigned char data[8];
} CanMessage;

void send_can_message(unsigned char can, CanMessage message) {

}

void test() {
    CanMessage message = {0x10,0x0,0x0, 0x0,0x0,0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7};
    send_can_message(1, message)
}