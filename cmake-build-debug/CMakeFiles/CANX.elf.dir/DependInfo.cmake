# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "ASM"
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_ASM
  "/Users/orungrau/Embedded/CANX/startup/startup_stm32f105xc.s" "/Users/orungrau/Embedded/CANX/cmake-build-debug/CMakeFiles/CANX.elf.dir/startup/startup_stm32f105xc.s.obj"
  )
set(CMAKE_ASM_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_ASM
  "STM32F105xC"
  "USE_HAL_DRIVER"
  )

# The include file search paths:
set(CMAKE_ASM_TARGET_INCLUDE_PATH
  "../Inc"
  "../Drivers/STM32F1xx_HAL_Driver/Inc"
  "../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy"
  "../Drivers/CMSIS/Device/ST/STM32F1xx/Include"
  "../Drivers/CMSIS/Include"
  "../lib"
  )
set(CMAKE_DEPENDS_CHECK_C
  "/Users/orungrau/Embedded/CANX/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal.c" "/Users/orungrau/Embedded/CANX/cmake-build-debug/CMakeFiles/CANX.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal.c.obj"
  "/Users/orungrau/Embedded/CANX/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_can.c" "/Users/orungrau/Embedded/CANX/cmake-build-debug/CMakeFiles/CANX.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_can.c.obj"
  "/Users/orungrau/Embedded/CANX/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_cortex.c" "/Users/orungrau/Embedded/CANX/cmake-build-debug/CMakeFiles/CANX.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_cortex.c.obj"
  "/Users/orungrau/Embedded/CANX/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_dma.c" "/Users/orungrau/Embedded/CANX/cmake-build-debug/CMakeFiles/CANX.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_dma.c.obj"
  "/Users/orungrau/Embedded/CANX/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_exti.c" "/Users/orungrau/Embedded/CANX/cmake-build-debug/CMakeFiles/CANX.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_exti.c.obj"
  "/Users/orungrau/Embedded/CANX/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_flash.c" "/Users/orungrau/Embedded/CANX/cmake-build-debug/CMakeFiles/CANX.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_flash.c.obj"
  "/Users/orungrau/Embedded/CANX/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_flash_ex.c" "/Users/orungrau/Embedded/CANX/cmake-build-debug/CMakeFiles/CANX.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_flash_ex.c.obj"
  "/Users/orungrau/Embedded/CANX/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_gpio.c" "/Users/orungrau/Embedded/CANX/cmake-build-debug/CMakeFiles/CANX.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_gpio.c.obj"
  "/Users/orungrau/Embedded/CANX/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_gpio_ex.c" "/Users/orungrau/Embedded/CANX/cmake-build-debug/CMakeFiles/CANX.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_gpio_ex.c.obj"
  "/Users/orungrau/Embedded/CANX/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_pwr.c" "/Users/orungrau/Embedded/CANX/cmake-build-debug/CMakeFiles/CANX.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_pwr.c.obj"
  "/Users/orungrau/Embedded/CANX/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_rcc.c" "/Users/orungrau/Embedded/CANX/cmake-build-debug/CMakeFiles/CANX.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_rcc.c.obj"
  "/Users/orungrau/Embedded/CANX/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_rcc_ex.c" "/Users/orungrau/Embedded/CANX/cmake-build-debug/CMakeFiles/CANX.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_rcc_ex.c.obj"
  "/Users/orungrau/Embedded/CANX/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_tim.c" "/Users/orungrau/Embedded/CANX/cmake-build-debug/CMakeFiles/CANX.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_tim.c.obj"
  "/Users/orungrau/Embedded/CANX/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_tim_ex.c" "/Users/orungrau/Embedded/CANX/cmake-build-debug/CMakeFiles/CANX.elf.dir/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_tim_ex.c.obj"
  "/Users/orungrau/Embedded/CANX/Src/main.c" "/Users/orungrau/Embedded/CANX/cmake-build-debug/CMakeFiles/CANX.elf.dir/Src/main.c.obj"
  "/Users/orungrau/Embedded/CANX/Src/stm32f1xx_hal_msp.c" "/Users/orungrau/Embedded/CANX/cmake-build-debug/CMakeFiles/CANX.elf.dir/Src/stm32f1xx_hal_msp.c.obj"
  "/Users/orungrau/Embedded/CANX/Src/stm32f1xx_it.c" "/Users/orungrau/Embedded/CANX/cmake-build-debug/CMakeFiles/CANX.elf.dir/Src/stm32f1xx_it.c.obj"
  "/Users/orungrau/Embedded/CANX/Src/syscalls.c" "/Users/orungrau/Embedded/CANX/cmake-build-debug/CMakeFiles/CANX.elf.dir/Src/syscalls.c.obj"
  "/Users/orungrau/Embedded/CANX/Src/system_stm32f1xx.c" "/Users/orungrau/Embedded/CANX/cmake-build-debug/CMakeFiles/CANX.elf.dir/Src/system_stm32f1xx.c.obj"
  "/Users/orungrau/Embedded/CANX/lib/CoreCAN.c" "/Users/orungrau/Embedded/CANX/cmake-build-debug/CMakeFiles/CANX.elf.dir/lib/CoreCAN.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "STM32F105xC"
  "USE_HAL_DRIVER"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../Inc"
  "../Drivers/STM32F1xx_HAL_Driver/Inc"
  "../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy"
  "../Drivers/CMSIS/Device/ST/STM32F1xx/Include"
  "../Drivers/CMSIS/Include"
  "../lib"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
