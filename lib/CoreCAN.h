//
// Created by Anatoly Myaskov on 26/01/2020.
//

#ifndef CANX_CORECAN_H
#define CANX_CORECAN_H

#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal_can.h"

HAL_StatusTypeDef HAL_CAN_Unsafe_SendData(CAN_HandleTypeDef *hal_can, CAN_TxHeaderTypeDef *pHeader, const uint8_t aData[], uint32_t *pTxMailbox);

#endif //CANX_CORECAN_H
