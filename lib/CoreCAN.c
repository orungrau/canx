//
// Created by Anatoly Myaskov on 26/01/2020.
//

#include "CoreCAN.h"

HAL_StatusTypeDef HAL_CAN_Unsafe_SendData(CAN_HandleTypeDef *hal_can, CAN_TxHeaderTypeDef *pHeader, const uint8_t aData[], uint32_t *pTxMailbox)
{
    uint32_t transmitMailBox;
    HAL_CAN_StateTypeDef state = hal_can->State;
    uint32_t tsr = READ_REG(hal_can->Instance->TSR);

    if ((state == HAL_CAN_STATE_READY) || (state == HAL_CAN_STATE_LISTENING))
    {
        /* Check that all the Tx mailboxes are not full */
        if (((tsr & CAN_TSR_TME0) != 0U) ||
            ((tsr & CAN_TSR_TME1) != 0U) ||
            ((tsr & CAN_TSR_TME2) != 0U))
        {
            /* Select an empty transmit mailbox */
            transmitMailBox = (tsr & CAN_TSR_CODE) >> CAN_TSR_CODE_Pos;

            /* Check transmit mailbox value */
            if (transmitMailBox > 2U)
            {
                /* Update error code */
                hal_can->ErrorCode |= HAL_CAN_ERROR_INTERNAL;
                return HAL_ERROR;
            }

            /* Store the Tx mailbox */
            *pTxMailbox = (uint32_t)1 << transmitMailBox;

            /* Set up the Id */
            if (pHeader->IDE == CAN_ID_STD)
            {
                hal_can->Instance->sTxMailBox[transmitMailBox].TIR = ((pHeader->StdId << CAN_TI0R_STID_Pos) | pHeader->RTR);
            }
            else
            {
                hal_can->Instance->sTxMailBox[transmitMailBox].TIR = ((pHeader->ExtId << CAN_TI0R_EXID_Pos) | pHeader->IDE | pHeader->RTR);
            }

            /* Set up the DLC */
            hal_can->Instance->sTxMailBox[transmitMailBox].TDTR = (pHeader->DLC);

            /* Set up the Transmit Global Time mode */
            if (pHeader->TransmitGlobalTime == ENABLE)
            {
                SET_BIT(hal_can->Instance->sTxMailBox[transmitMailBox].TDTR, CAN_TDT0R_TGT);
            }

            /* Set up the data field */
            WRITE_REG(hal_can->Instance->sTxMailBox[transmitMailBox].TDHR,
                      ((uint32_t)aData[7] << CAN_TDH0R_DATA7_Pos) |
                      ((uint32_t)aData[6] << CAN_TDH0R_DATA6_Pos) |
                      ((uint32_t)aData[5] << CAN_TDH0R_DATA5_Pos) |
                      ((uint32_t)aData[4] << CAN_TDH0R_DATA4_Pos));
            WRITE_REG(hal_can->Instance->sTxMailBox[transmitMailBox].TDLR,
                      ((uint32_t)aData[3] << CAN_TDL0R_DATA3_Pos) |
                      ((uint32_t)aData[2] << CAN_TDL0R_DATA2_Pos) |
                      ((uint32_t)aData[1] << CAN_TDL0R_DATA1_Pos) |
                      ((uint32_t)transmitMailBox << CAN_TDL0R_DATA0_Pos));

            /* Request transmission */
            SET_BIT(hal_can->Instance->sTxMailBox[transmitMailBox].TIR, CAN_TI0R_TXRQ);
            /* Return function status */
            return HAL_OK;
        }
        else
        {
            /* Update error code */
            hal_can->ErrorCode |= HAL_CAN_ERROR_PARAM;
            return HAL_ERROR;
        }
    }
    else
    {
        /* Update error code */
        hal_can->ErrorCode |= HAL_CAN_ERROR_NOT_INITIALIZED;
        return HAL_ERROR;
    }
}
